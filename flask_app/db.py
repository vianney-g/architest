from typing import Iterable

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    MetaData,
    Numeric,
    String,
    Table,
    UniqueConstraint,
)
from sqlalchemy.orm import clear_mappers, mapper
from sqlalchemy.orm.exc import NoResultFound

import domain.db as d_db
from domain import ExchangeRate, ExchangeRateExists, ExchangeRateNotFound

database = SQLAlchemy()
metadata = MetaData()


currency = Table(
    "currency",
    metadata,
    Column("iso_code", String, primary_key=True),
)

rate = Table(
    "exchange_rate",
    metadata,
    Column("id", Integer, primary_key=True, autoincrement=True),
    Column("change", Numeric(precision=10, scale=5)),
    Column("from_cur_id", String, ForeignKey("currency.iso_code")),
    Column("to_cur_id", String, ForeignKey("currency.iso_code")),
    UniqueConstraint("from_cur_id", "to_cur_id"),
)


def start_mappers():
    """Map Tables to domain objects"""
    clear_mappers()
    mapper(
        ExchangeRate,
        rate,
        properties={
            "currency": rate.c.to_cur_id,
            "euro_rate": rate.c.change,
        },
        polymorphic_on=rate.c.from_cur_id,
        polymorphic_identity="EUR",
    )


def init_app(app):
    """Must be called on app init before any db call"""
    start_mappers()
    database.init_app(app)


class ExchangeRateRepository(d_db.ExchangeRateRepository):
    def __init__(self, session):
        self._session = session
        self._q = self._session.query(ExchangeRate).filter_by(from_cur_id="EUR")

    def get(self, currency: str) -> ExchangeRate:
        try:
            return self._q.filter_by(currency=currency).one()
        except NoResultFound:
            raise ExchangeRateNotFound(currency)

    def add(self, rate: ExchangeRate):
        self._session.add(rate)
        try:
            self._session.commit()
        except Exception as e:
            raise ExchangeRateExists(rate) from e

    def update(self, rate: ExchangeRate):
        self._session.merge(rate)
        try:
            self._session.commit()
        except Exception as e:
            print(e)
            raise ExchangeRateExists(rate) from e

    def list(self) -> Iterable[ExchangeRate]:
        return self._q.all()

    def __contains__(self, rate: ExchangeRate) -> bool:
        q = self._q.filter(ExchangeRate.currency == rate.currency)
        return self._session.query(q.exists())

    def __len__(self) -> int:
        return self._q.count()
