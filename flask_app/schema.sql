DROP TABLE IF EXISTS "conv_app_exchangerate";


CREATE TABLE "conv_app_exchangerate" (
	"iso_cur" varchar(3) NOT NULL PRIMARY KEY,
	"euro_change" decimal NOT NULL
);
