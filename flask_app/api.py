import os

from flask import Flask, abort, jsonify, request

from services import convert_from_str

from . import db


def convert_money():
    try:
        query = request.json["query"]
    except (KeyError, AttributeError, TypeError):
        abort(400)

    try:
        repository = db.ExchangeRateRepository(db.database.session)
        answer = convert_from_str(query, repository)
    except ValueError:
        abort(400)

    return jsonify({"answer": str(answer)})


def create_app(conf):
    """Flask app factory"""
    app = Flask(__name__)

    try:
        db_path = os.environ["ARCHITEST_DB_PATH"]
    except KeyError as ke:
        raise RuntimeError(
            "Please set ARCHITEST_DB_PATH env var." " Must be a path to a sqlite3 file."
        ) from ke

    app.config["SQLALCHEMY_DATABASE_URI"] = f"sqlite:///{db_path}"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    try:
        app.config.from_mapping(conf)
    except TypeError:
        pass

    app.route("/money/convert", methods=["POST"])(convert_money)

    db.init_app(app)

    return app
