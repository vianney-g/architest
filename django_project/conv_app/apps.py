from django.apps import AppConfig


class ConvAppConfig(AppConfig):
    name = "conv_app"
