from django.db import models

# We create two tables for demonstration purpose.
# In our domain model, Exchange rate is stupid simple (one currency, one rate)

# But in real life, domain model may be unrelated to DB tables
# it makes sense that all currencies are stored in one table, and exchange rates in another.


class Currency(models.Model):
    iso_code = models.CharField(max_length=3, primary_key=True)

    class Meta:
        db_table = "currency"


class EuroExchangeRate(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(from_cur__iso_code="EUR")


class ExchangeRate(models.Model):
    from_cur = models.ForeignKey(
        Currency, on_delete=models.CASCADE, related_name="exchange_from"
    )
    to_cur = models.ForeignKey(
        Currency, on_delete=models.CASCADE, related_name="exchange_to"
    )
    change = models.DecimalField(max_digits=10, decimal_places=5, null=False)

    from_euro = EuroExchangeRate()

    class Meta:
        unique_together = [("from_cur", "to_cur")]
        db_table = "exchange_rate"
