from django.core.management.base import BaseCommand, CommandError

from django_project.conv_app.adapters import ExchangeRateRepository
from services import populate

DEFAULT_SOURCE = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml"


class Command(BaseCommand):
    help = "Import exchange rates from an url"

    def add_arguments(self, parser):
        parser.add_argument(
            "--source",
            "-s",
            default=DEFAULT_SOURCE,
            help="Source file (from an URL or file system path)",
        )
        parser.add_argument(
            "--dry-run",
            action="store_true",
            help="Do not store read data in DB",
        )

    def handle(self, *args, **options):
        source, dry_run = options["source"], options["dry_run"]

        try:
            populate(source, into=ExchangeRateRepository(), dry_run=dry_run)
        except Exception as err:
            raise CommandError(err) from err

        return "Exchange rates successfuly imported"
