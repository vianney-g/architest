from typing import Iterable

from django.db import transaction

import domain.db as db
from domain import ExchangeRate

from .models import Currency
from .models import ExchangeRate as DjangoExchangeRate


def _from_django(rate: DjangoExchangeRate) -> ExchangeRate:
    """Convert a django model exchange rate to domain ExchangeRate"""
    return ExchangeRate(rate.to_cur.iso_code, rate.change)


def _save(rate: ExchangeRate):
    """Transform rate and save it in django db"""
    euro, _ = Currency.objects.get_or_create(iso_code="EUR")
    currency, _ = Currency.objects.get_or_create(iso_code=rate.currency)
    DjangoExchangeRate.from_euro.update_or_create(
        to_cur=currency,
        change=rate.euro_rate,
        from_cur=euro,
    )


class ExchangeRateRepository(db.ExchangeRateRepository):
    def get(self, currency: str) -> ExchangeRate:
        try:
            django_rate = DjangoExchangeRate.from_euro.get(to_cur__iso_code=currency)
        except DjangoExchangeRate.DoesNotExist as does_not_exist:
            raise db.ExchangeRateNotFound(currency) from does_not_exist
        return _from_django(django_rate)

    def add(self, rate: ExchangeRate):
        if rate in self:
            raise db.ExchangeRateExists(rate)
        _save(rate)

    def update(self, rate: ExchangeRate):
        if rate not in self:
            raise db.ExchangeRateNotFound(rate)
        _save(rate)

    def list(self) -> Iterable[ExchangeRate]:
        for django_rate in DjangoExchangeRate.from_euro.all():
            yield _from_django(django_rate)

    def __contains__(self, rate: ExchangeRate) -> bool:
        return DjangoExchangeRate.from_euro.filter(
            to_cur__iso_code=rate.currency
        ).exists()

    def __len__(self) -> int:
        return DjangoExchangeRate.from_euro.count()
