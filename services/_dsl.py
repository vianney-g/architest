"""Service to deal with a small DSL"""
from decimal import Decimal
from typing import Iterable, Tuple

import ply.lex as lex
import ply.yacc as yacc

tokens = ["IN", "CURRENCY", "AMOUNT"]

t_IN = r"(?i:in)"
t_ignore = " \t"


def t_CURRENCY(t):
    r"[a-zA-Z]{3}"
    t.value = t.value.upper()
    return t


def t_AMOUNT(t):
    r"\d+(\.\d*)?"
    t.value = Decimal(t.value)
    return t


def t_error(t):
    raise ValueError(f"I can't understand {t.value[0]}")


def p_expression(p):
    "expression : AMOUNT CURRENCY IN CURRENCY"
    p[0] = p[1], p[2], p[4]


def p_error(p):
    raise ValueError(f"I can't parse {p}")


_lexer = lex.lex()
_parser = yacc.yacc()


def parse(request: str) -> Tuple[Decimal, str, str]:
    """Parse the request and return
    (requested_amount, from_currency, to_currency)
    """
    return _parser.parse(request)
