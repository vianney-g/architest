from io import IOBase
from typing import Callable, Iterable
from xml.etree import ElementTree

from domain import ExchangeRate


def parse(xml_file: IOBase, parser=ElementTree.parse) -> Iterable[ExchangeRate]:
    """Extract exchange rates from xml

    :param xml_file: path or file like object
    """
    tree = parser(xml_file)
    rates_xpath = ".//{*}Cube[@currency][@rate]"

    element: ElementTree.Element
    for element in tree.iterfind(rates_xpath):
        yield ExchangeRate.from_literal(
            element.get("currency", ""), element.get("rate", "")
        )
