import itertools
import pathlib
import urllib.request as request
from functools import singledispatch
from io import IOBase
from typing import Iterable, Union

from domain import EURO, ExchangeRate, ExchangeRateExists, ExchangeRateRepository

from . import xml_reader


@singledispatch
def populate(
    rates: Iterable[ExchangeRate],
    into: ExchangeRateRepository,
    dry_run=False,
):
    for rate in itertools.chain([EURO], rates):
        if not dry_run:
            try:
                into.add(rate)
            except ExchangeRateExists:
                into.update(rate)


@populate.register
def populate_from_file(
    xml_file: IOBase,
    into: ExchangeRateRepository,
    dry_run=False,
):
    """Populate xml data into given repo"""
    return populate(xml_reader.parse(xml_file), into, dry_run)


@populate.register
def populate_from_request(
    req: request.Request,
    into: ExchangeRateRepository,
    dry_run=False,
):
    """Populate from a request.
    Ok it's a lot of decoupling but.. who knows?
    Maybe populate_from_url is not enough is data should be retrieved from a POST request?
    """
    response = request.urlopen(req)
    return populate(response, into, dry_run)


@populate.register
def populate_from_path(
    path: pathlib.Path,
    into: ExchangeRateRepository,
    dry_run=False,
):
    with path.open() as file_:
        return populate(file_, into, dry_run)


@populate.register
def populate_from_string(
    path_or_url: str,
    into: ExchangeRateRepository,
    dry_run=False,
):
    """Fetch data from the internet. Url should return a valid XML file"""
    source: Union[pathlib.Path, request.Request]
    try:
        source = request.Request(path_or_url)
    except ValueError:
        source = pathlib.Path(path_or_url)

    return populate(source, into, dry_run)
