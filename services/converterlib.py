from decimal import Decimal

from domain import ExchangeRate, ExchangeRateRepository

from ._dsl import parse

_precision = Decimal(".01")


def convert(
    amount: Decimal,
    repo: ExchangeRateRepository,
    *,
    from_currency: str,
    to_currency: str
) -> Decimal:
    """Convert an amount in another currency

    :raises: RateDoesNotExist
    """
    from_rate: ExchangeRate = repo.get(from_currency)
    to_rate: ExchangeRate = repo.get(to_currency)

    amount = amount.quantize(_precision)

    eur_amount = amount / from_rate.euro_rate
    result = eur_amount.quantize(_precision) * to_rate.euro_rate
    return result.quantize(_precision)


def convert_from_str(input_str: str, repo: ExchangeRateRepository):
    """Convert money from a valid string input

    :example: "13 EUR in USD"
    :raises: ValueError if string cannot be parsed.
    """
    amount, from_currency, to_currency = parse(input_str)
    return convert(amount, repo, from_currency=from_currency, to_currency=to_currency)
