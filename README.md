### DESCRIPTION

Simple money converter.

## MOTIVATIONS

Job interview tests. I tried to follow best practices and hard decoupling.
Developed following Domain Driven Development principles.

That's why Django is a project dependency (i.e project does not depend on Django)

For demonstration purposes the API runs with Flask, to show we can easily switch the ORM
(and to appreciate SQLAlchemy mappings).

DB model is intentionaly complicated to break the mirror between tables structure and domain models.
Again, demonstration purposes.

## REQUIREMENTS
Run on *python 3.8*

Check you version is ok and sqlite is installed with `make check`

## INSTALLATION & TESTS

In a python 3.8 environment, run `make install`.
It will install requirements, create the db and populate it.

To run tests suite, execute `make tests`
To run `mypy` types checks, execute `mypy ./`

## USAGE

Start the api with `make run`

Request it, e.g:

`curl -X POST http://127.0.0.1:5000/money/convert --data '{"query": "1337.25 EUR in USD"}' --header "Content-Type: application/json"`


## LICENSE & CREDITS

Vianney Gremmel vianney.gremmel@gmail.com

All rights reserved.
