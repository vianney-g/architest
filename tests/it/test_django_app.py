import pathlib

import pytest
from django.core.management import call_command

from django_project.conv_app.models import ExchangeRate as DjangoRates


@pytest.mark.django_db
def test_import_currencies():
    xml_test_path = pathlib.Path(__file__).parent / "sample_data.xml"
    call_command("import_currencies", source=str(xml_test_path))

    assert DjangoRates.from_euro.count() == 4
