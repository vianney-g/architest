from unittest import mock

import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from flask_app import api
from flask_app.db import ExchangeRateRepository, database, metadata, start_mappers


@pytest.fixture
def in_memory_db():
    """sqlite in memory for tests"""
    engine = create_engine("sqlite:///:memory:")
    metadata.create_all(engine)
    yield engine
    metadata.drop_all(engine)


@pytest.fixture
def db_session(in_memory_db):
    start_mappers()
    yield sessionmaker(bind=in_memory_db)()


@pytest.fixture
def alchemy_repo(db_session):
    return ExchangeRateRepository(db_session)


@pytest.fixture
def client(in_memory_db):
    """A flask client test"""
    test_conf = {
        "SQLALCHEMY_DATABASE_URI": "sqlite:///:memory:",
        "TESTING": True,
    }
    app = api.create_app(test_conf)

    with app.test_client() as client:
        with app.app_context():
            with mock.patch.object(database, "get_engine", return_value=in_memory_db):
                yield client
