from domain import ExchangeRate


def test_mapper_can_load_exchange_rate(db_session):
    db_session.execute("INSERT INTO currency VALUES ('EUR'), ('USD')")
    db_session.execute("INSERT INTO exchange_rate VALUES(2, 1.20, 'EUR', 'USD')")

    expected = [ExchangeRate.from_literal("USD", "1.20")]

    assert expected == db_session.query(ExchangeRate).all()


def test_we_can_request_repository(alchemy_repo):
    usd_rate = ExchangeRate.from_literal("USD", "1.20")
    alchemy_repo.add(usd_rate)
    assert usd_rate == alchemy_repo.get("USD")
    assert usd_rate in alchemy_repo
    assert len(alchemy_repo) == 1


def test_other_than_euro_is_filtered(alchemy_repo):
    alchemy_repo._session.execute(
        "INSERT INTO currency VALUES ('EUR'), ('USD'), ('CHF')"
    )
    alchemy_repo._session.execute(
        "INSERT INTO exchange_rate VALUES(2, 1.20, 'CHF', 'USD')"
    )

    assert len(alchemy_repo) == 0


def test_404_response(client):
    response = client.get("/")
    assert response.status_code == 404


def test_invalid_method(client):
    response = client.get("/money/convert")
    assert response.status_code == 405


def test_post_no_data(client):
    response = client.post("/money/convert")
    assert response.status_code == 400


def test_post_invalid_data(client):
    response = client.post(
        "/money/convert",
        data='{"wtf": "bad input"}',
        headers={"Content-Type": "application/json"},
    )
    assert response.status_code == 400


def test_post_bad_header(client):
    response = client.post(
        "/money/convert",
        data='{"query": "1337 EUR in USD"}',
        headers={"Content-Type": "application/wtf"},
    )
    assert response.status_code == 400


def test_post_bad_query(client):
    response = client.post(
        "/money/convert",
        data='{"query": "convert 1337 beans to spams"}',
        headers={"Content-Type": "application/json"},
    )
    assert response.status_code == 400


def test_post_good_query(client, alchemy_repo):
    usd_rate = ExchangeRate.from_literal("USD", "1.20")
    eur_rate = ExchangeRate.from_literal("EUR", "1")
    alchemy_repo.add(usd_rate)
    alchemy_repo.add(eur_rate)
    response = client.post(
        "/money/convert",
        data='{"query": "1337 eur in usd"}',
        headers={"Content-Type": "application/json"},
    )
    assert response.status_code == 200
    assert response.json == {"answer": "1604.40"}
