import pathlib
from decimal import Decimal

import pytest

import domain.db.memory as memory
from domain import ExchangeRate


@pytest.fixture
def fake_repo():
    return memory.ExchangeRateRepository()


EQUIVALENT_AMOUNTS = (
    ("EUR", "10.32"),
    ("USD", "12.51"),
    ("AUD", "16.20"),
    ("CHF", "11.15"),
)


@pytest.fixture(
    params=EQUIVALENT_AMOUNTS,
    ids=[currency for currency, _ in EQUIVALENT_AMOUNTS],
)
def equivalent_amount(request):
    currency, amount = request.param
    return (currency, Decimal(amount))


# trick for cartesien product of fixtures
equivalent_amount_2 = equivalent_amount


@pytest.fixture
def initialized_fake_repo():
    rates = [
        ExchangeRate.from_literal("EUR", "1"),
        ExchangeRate.from_literal("CHF", "1.08"),
        ExchangeRate.from_literal("USD", "1.2124"),
        ExchangeRate.from_literal("AUD", "1.57"),
    ]
    return memory.ExchangeRateRepository(rates)


@pytest.fixture
def xml_file_path() -> pathlib.Path:
    return pathlib.Path(__file__).parent / "sample_data.xml"


@pytest.fixture
def xml_file(xml_file_path):
    with xml_file_path.open() as _file:
        yield _file
