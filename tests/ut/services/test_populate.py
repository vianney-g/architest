from unittest import mock

import pytest

from domain import ExchangeRate
from services import populate


def test_we_can_populate_a_db_from_xml(xml_file, fake_repo):
    populate(xml_file, into=fake_repo)
    assert fake_repo.get("USD")
    assert len(fake_repo) == 4


def test_dry_run_population(xml_file, fake_repo):
    populate(xml_file, into=fake_repo, dry_run=True)
    assert len(fake_repo) == 0


@mock.patch("services.populatelib.request.urlopen")
def test_we_can_populate_from_an_url(mocked_response, fake_repo, xml_file):
    mocked_response.return_value = xml_file
    populate("http://localhost/data", into=fake_repo)
    assert len(fake_repo) == 4


def test_we_can_populate_from_a_path(fake_repo, xml_file_path):
    populate(str(xml_file_path), fake_repo)
    assert len(fake_repo) == 4


def test_population_update_existing_rate(xml_file, fake_repo):
    initial_rate = ExchangeRate.from_literal("USD", "123")
    fake_repo.add(initial_rate)
    assert initial_rate in fake_repo

    populate(xml_file, fake_repo)
    expected_rate = ExchangeRate.from_literal("USD", "1.2124")
    assert fake_repo.get("USD") == expected_rate


def test_EUR_is_always_inserted(fake_repo):
    populate([], fake_repo)

    euro = fake_repo.get("EUR")
    assert euro.euro_rate == 1
