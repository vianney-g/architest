from decimal import Decimal

import pytest

from services._dsl import parse


@pytest.mark.parametrize(
    "invalid_input",
    [
        "EUR in USD",
        "EUR in USD 13",
        "12 EUR TO USD",
        "12 EURO IN USD",
    ],
)
def test_invalid_conversion(invalid_input):
    with pytest.raises(ValueError):
        parse(invalid_input)


@pytest.mark.parametrize(
    "valid_input, expected",
    [
        ("10.32 EUR IN USD", (Decimal("10.32"), "EUR", "USD")),
        ("10. usd in chf", (Decimal("10."), "USD", "CHF")),
        ("10 EUR iN Eur", (Decimal("10"), "EUR", "EUR")),
        ("   10 EUR   iN WTF  ", (Decimal("10"), "EUR", "WTF")),
    ],
)
def test_valid_input(valid_input, expected):
    assert expected == parse(valid_input)
