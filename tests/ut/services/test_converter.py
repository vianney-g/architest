from decimal import Decimal

import pytest

from services import convert, convert_from_str


@pytest.mark.parametrize("from_human_parsable", [True, False])
def test_we_can_convert(
    from_human_parsable,
    initialized_fake_repo,
    equivalent_amount,
    equivalent_amount_2,
):
    from_cur, from_amount = equivalent_amount
    to_cur, to_amount = equivalent_amount_2

    if from_human_parsable:
        human_input = f"{from_amount} {from_cur} IN {to_cur}"
        result = convert_from_str(human_input, repo=initialized_fake_repo)
    else:
        result = convert(
            from_amount,
            repo=initialized_fake_repo,
            from_currency=from_cur,
            to_currency=to_cur,
        )

    assert Decimal(to_amount) == result


def test_we_can_convert_same_currency(initialized_fake_repo, equivalent_amount):
    from_cur, from_amount = equivalent_amount

    assert from_amount == convert(
        from_amount,
        repo=initialized_fake_repo,
        from_currency=from_cur,
        to_currency=from_cur,
    )


def test_convert_from_invalid_string_raises_a_value_error(fake_repo):
    with pytest.raises(ValueError):
        convert_from_str("INVALID", fake_repo)
