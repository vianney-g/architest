PYTHON_VERSION = 3.8

export PYTHONPATH = $(shell pwd)
export DJANGO_SETTINGS_MODULE = django_project.currency_conv.settings
export ARCHITEST_DB_PATH = $(shell pwd)/architest.sqlite3

.DEFAULT : help
.PHONY : help check install tests


help:
	@echo "---------------------- HELP -------------------------"
	@echo "Launch 'make check' to check your python version.    "
	@echo "Launch 'make install' to install python dependencies."
	@echo "Launch 'make tests' to launch tests.                 "
	@echo "Launch 'make run' to launch local server.            "
	@echo "-----------------------------------------------------"

_check_sqlite3:
	@[ -x "$$(command -v sqlite3)" ] && exit 0 || echo "Please install sqlite3" && exit 1

.ONESHELL:
_check_python:
	@py_version=$$(python -V | cut -d" " -f2 | cut -d. -f1,2)
	@[ "$$py_version" = $(PYTHON_VERSION) ] && exit 0
	@echo "Bad python version. Work only with python 3.8." \
		"Please setup a python3.8 env first" && exit 1

check: _check_sqlite3 _check_python
	@echo "Everything clean"

_pip_install: _check_python
	@python -m pip install -r requirements.txt

_make_migrations:
	@./django_project/manage.py makemigrations conv_app

_migrate: _make_migrations
	@./django_project/manage.py migrate

install: _pip_install _migrate
	@./django_project/manage.py import_currencies

tests: 
	@python -m pytest

run:
	@FLASK_APP="flask_app/api.py" flask run
