from .db import ExchangeRateExists, ExchangeRateNotFound, ExchangeRateRepository
from .models import ExchangeRate

EURO = ExchangeRate.from_literal("EUR", "1")
