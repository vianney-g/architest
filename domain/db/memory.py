from typing import Iterable

from domain import ExchangeRate

from . import base
from ..models import ExchangeRate


class ExchangeRateRepository(base.ExchangeRateRepository):
    """In memory repository.

    Useful for tests
    """

    def __init__(self, initial_rates: Iterable[ExchangeRate] = None):
        self._rates = (
            {rate.currency: rate for rate in initial_rates} if initial_rates else {}
        )

    def get(self, currency: str) -> ExchangeRate:
        try:
            return self._rates[currency]
        except KeyError as key_err:
            raise base.ExchangeRateNotFound from key_err

    def add(self, rate: ExchangeRate):
        if rate in self:
            raise base.ExchangeRateExists
        self._rates[rate.currency] = rate

    def update(self, rate: ExchangeRate):
        if rate not in self:
            raise base.ExchangeRateNotFound
        self._rates[rate.currency] = rate

    def list(self):
        return self._rates.values()

    def __contains__(self, rate: ExchangeRate):
        return rate.currency in self._rates

    def __len__(self) -> int:
        return len(self._rates)
