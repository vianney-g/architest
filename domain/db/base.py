import abc
from typing import Iterable

from ..models import ExchangeRate


class ExchangeRateNotFound(Exception):
    pass


class ExchangeRateExists(Exception):
    pass


class ExchangeRateRepository(abc.ABC):
    """Abstract class for ExchangeRate repositories"""

    @abc.abstractmethod
    def get(self, currency: str) -> ExchangeRate:
        """
        :raises: ExchangeRateNotFound
        """

    @abc.abstractmethod
    def add(self, rate: ExchangeRate):
        """
        :raises: ExchangeRateExists
        """

    @abc.abstractmethod
    def update(self, rate: ExchangeRate):
        """
        :raises: ExchangeRateNotFound
        """

    @abc.abstractmethod
    def list(self) -> Iterable[ExchangeRate]:
        ...

    @abc.abstractmethod
    def __contains__(self, rate: ExchangeRate) -> bool:
        ...

    @abc.abstractmethod
    def __len__(self) -> int:
        ...

    def __iter__(self) -> Iterable[ExchangeRate]:
        return iter(self.list())
