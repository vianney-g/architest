import dataclasses
import decimal


@dataclasses.dataclass
class ExchangeRate:
    """Domain model that represents a simple exchange rate"""

    currency: str
    euro_rate: decimal.Decimal

    @classmethod
    def from_literal(cls, currency: str, rate: str):
        return cls(currency, decimal.Decimal(rate))
